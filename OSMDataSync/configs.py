from .fileloader import found_config_file, load_valid_config_file, get_config_file_path
from .helper import field_names


class Configuration:
    def __init__(self, layer):
        self.layer = layer
        self.config_dict = None
        self.state_field_name = None
        self.osm_id_attribute = None
        self.primary_key_name = None

    def set_configurations(self):
        configs = self.config_dict[1]
        self.state_field_name = configs["sync_state_attribute"]
        self.osm_id_attribute = configs["osm_id_attribute"]
        self.primary_key_name = configs["primary_key"]

    def config_file_state(self):
        if found_config_file(self.layer):
            self.config_dict = load_valid_config_file(get_config_file_path(self.layer))
            fields = field_names(self.layer)
            if (
                self.config_dict[0]
                and self.config_dict[1]["sync_state_attribute"] in fields
                and self.config_dict[1]["osm_id_attribute"] in fields
                and self.config_dict[1]["primary_key"] in fields
            ):
                self.set_configurations()
                return "Ok", self.config_dict
            else:
                return "invalid", self.config_dict
        else:
            return "Not found", None
