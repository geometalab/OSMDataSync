from qgis.core import QgsWkbTypes
SIZE = 0.00001


def convert_to_layer_type(layer, geo_json_feature_collection):
    if layer.geometryType() == QgsWkbTypes.PolygonGeometry:
        return convert_to_polygon(geo_json_feature_collection)
    else:
        return geo_json_feature_collection


def convert_to_polygon(json):
    for osm_object in json["features"]:
        if osm_object["geometry"]["type"] == "Point":
            osm_object["geometry"]["type"] = "Polygon"

            lon, lat = osm_object["geometry"]["coordinates"]
            north_west_corner = [lon - SIZE, lat + SIZE]
            north_east_corner = [lon + SIZE, lat + SIZE]
            south_east_corner = [lon + SIZE, lat - SIZE]
            south_west_corner = [lon - SIZE, lat - SIZE]

            outer_ring = [
                north_west_corner,
                north_east_corner,
                south_east_corner,
                south_west_corner,
                north_west_corner,
            ]
            rings = [outer_ring]
            osm_object["geometry"]["coordinates"] = rings
    return json
